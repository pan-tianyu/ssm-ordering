
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
		<style>
			.td{
				text-align: right;
			}
		</style>
	</head>
	<body>
	<form action="${pageContext.request.contextPath}/customer/toUpdateCustomer" method="post">
		<div class="tr">
			<table border="2px" cellspacing="0" style="list-style:none; width:700px;margin: auto;border: 1px #FF0000 solid;border-radius: 4px;">
				<tr>
					<td colspan="3" align="center">请填写用户信息(带*为必填项)</td>
				</tr>
				<tr>
					<td class="td">序号：</td>
					<td><input name="id" type="text" value="${customer.id }"></td>

				</tr>
				<tr>
					<td class="td">用户名：</td>
					<td><input name="account" type="text" value="${customer.account }"></td>
					<td>*你用来登录的用户名</td>
				</tr>
				<tr>
					<td >密码：</td>
					<td><input name="password" type="text" value="${customer.password }"></td>
					<td>*长度必须大于5小于16个字符，只能为英语字、数字</td>
				</tr>
				<tr>
					<td class="td">确认密码：</td>
					<td><input name="password" type="text" value="${customer.password }"></td>
					<td>*请将输入的密码再次输入</td>
				</tr>
				<tr>
					<td class="td">真实姓名：</td>
					<td><input name="name" type="text" value="${customer.name }"></td>
					<td>*请输入你的真实姓名</td>
				</tr>
				<tr>
					<td class="td">性别：</td>
					<input name="" type="radio" value="${customer.gender }">
					<td>男：<input type="radio" name="gender" value="男" />女：<input type="radio" name="gender" value="女" /></td>
					<td>*请填写你的真实信息</td>
				</tr>
				<tr>
					<td class="td">生日：</td>
					<td><input name="birthday" type="date" value="${customer.birthday }"></td>
					<td>*请填写你的真实年龄</td>
				</tr>
				<tr>
					<td class="td">身份证号：</td>
					<td><input name="idcard" type="text" min="0"maxlength="18" value="${customer.idcard }"></td>
					<td>*请填写你的真实信息</td>
				</tr>
				<tr>
					<td class="td">家庭住址：</td>
					<td><input name="address" type="text" value="${customer.address }"></td>
					<td>*请填写你的真实信息</td>
				</tr>
				<tr>
					<td class="td">电话号码：</td> 
					<td><input name="telephone" type="text" min="0"maxlength="11" value="${customer.telephone }"></td>
					<td>*请填写你的真实信息(格式为02487654321或1398765432)</td>
				</tr>
				<tr>
					<td class="td">电子邮箱：</td>
					<td><input name="email" type="text" value="${customer.email }"></td>
					<td>*请填写你有效的邮件地址，以便我们为你提供有效的服务</td>
				</tr>
				<tr>
				    <td class="td">邮政编码：</td>
					<td><input name="code" type="text" min="0" maxlength="6" value="${customer.code }"></td>
					<td>*请填写你的真实信息(格式为000000)</td>
				</tr>
				<tr>
					<td colspan="3" style="text-align: center;">
						<button type="submit">提交</button>
						<button >重置</button>
					</td>
					
				</tr>
			</table>
		</div>
	</form>
	</body>
</html>
