<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
		<style>
			.ul{
				list-style: none;
				text-align: center;
			}
			.ul a{
				text-decoration: none;	
			}
			.ul a:hover{
				color: darkorange;
			}
			.div{
				margin: auto;
				width: 850px;
			}
		</style>
	</head>
	<body>
	<form action="${pageContext.request.contextPath}/fore/delivery" method="post">
		<div class="div";>
			<h1 align="center">配送指南</h1>
			<p>午餐时间：11:00~14:00</p>
			<p>晚餐时间：17:00~21:00</p>
			<p>联系电话：4008888</p>
			<p>联系人：李经理</p>
			<p>注：本店不支持网上付款，费用将有送餐人员代收，请自备零钱，谢谢合作！</p>
			<p>订单经过本店客服确认后，将安排快递人员送餐，由于中午订餐人员较多，请在本店正常营业时间外订餐，到时才能准时送达！周末进店人数比较多，暂不支持订餐，请见谅！</p>
			<ul class="ul"><li><a href="home.jsp">返回</a></li></ul>
		</div>
	</form>
	</body>
</html>
