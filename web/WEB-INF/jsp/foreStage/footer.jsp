<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
		<link href="<%=request.getContextPath()%>/img/dingcanall.jpg" rel="stylesheet" type="text/img"/>
		<style>
			.footer{
				width: 850px;
				height: 50px;
				margin: auto;
			}
			.footernav{
				width: 850px;
				height: 50px;
				background-image: url(<%=request.getContextPath()%>/img/dingcanall.jpg);
			}
			.footer ul li{
				list-style: none;/*取消li标签前面的符号*/
				
			}
			.footer ul {
				margin-left:300px;
			}
			.footer ul li a:hover {
				color: orange;
			}
			.footernav ul li {
				float: left;
				list-style: none;
				margin: 3px;
				font-size:8px;
			}
			.footer ul p{
				text-align:center;
				font-size: 8px;
			}
			.footer a{
				text-decoration: none;
			}
		</style>
	</head>
	<body>
	<form action="${pageContext.request.contextPath}/fore/footer" method="post">
		<div class="footer">
				<nav class="footernav"><!--nav导航按钮-->
					<ul>
						<li>
							<a href="${pageContext.request.contextPath}/fore/aboutUs" target="article">关于我们</a> |
						</li>
						<li>
							<a href="${pageContext.request.contextPath}/fore/delivery" target="article">配送说明</a> |
						</li>
				        <br/>
				        <p style="margin-left: -380px">
					        Copyright(C)2015-2020
				        </p>
					</ul>	
				</nav>
				
		</div>
	</form>
	</body>
</html>
