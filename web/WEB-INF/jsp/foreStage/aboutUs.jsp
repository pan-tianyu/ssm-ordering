<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
		<style>
			.p{
				text-indent: 2em;
			}
			.ul{
				list-style: none;
				text-align: center;
			}
			.ul a{
				text-decoration: none;	
			}
			.ul a:hover{
				color: darkorange;
			}
			.div{
				margin: auto;
				width: 850px;
			}
		</style>
	</head>
	<body>
	<form action="${pageContext.request.contextPath}/fore/aboutUs" method="post">
		<div class="div">
			<h1 align="center">关于我们</h1>
			<p class="p">小时候的味道</p>
			<p class="p">欢迎来到</p>
			<ul class="ul"><li><a href="home.jsp">返回</a></li></ul>
		</div>
	</form>
	</body>
</html>
