<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
		<style>
			.table td{
				margin: auto;
				height: 30px;
				text-align: center;
			}
			.table button{
				margin-left: 20px;
			}
			.tr ul li a:hover {
				color: orange;
			}
			.tr ul li{
				list-style: none;/*去除li标签的点*/
			}
		</style>
	</head>
	<body>
	<form action="${pageContext.request.contextPath}/fore/detailOfDishes" method="post">
		<!--<ul style="list-style:none; border:1px #FF0000 solid; border-radius: 4px; width:100px;">
			<li>123456</li>
			<li>1231544</li>
		</ul>-->
		<table class="table" border="2" cellspacing="0" align="center">
			<tr>
				<td>菜单名称</td>
				<td>咸肉菜饭</td>
			</tr>
			<tr>
				<td>原料</td>
				<td></td>
			</tr>
			<tr>
				<td>市场单价</td>
				<td></td>
			</tr>
			<tr>
				<td>会员单价</td>
				<td></td>
			</tr>
			<tr>
				<td>说明</td>
				<td></td>
			</tr>
			<tr>
				<td>菜品类别</td>
				<td></td>
			</tr>
			<tr>
				<td>展示图片</td>
				<td></td>
			</tr>
			<tr>
				<td colspan="2">
					<nav class="tr"><ul><li><a>返回</a></li></ul></nav>
				</td>
			</tr>
		</table>
	</form>
	</body>
</html>
