<%--
  Created by IntelliJ IDEA.
  User: 潘天宇
  Date: 2021/12/27
  Time: 19:12
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>管理员列表</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- 引入 Bootstrap -->
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<div class="container">

    <div class="row clearfix">
        <div class="col-md-12 column">
            <div class="page-header">
                <h1>
                    <small>管理员列表 —— 显示所有管理员</small>
                </h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 column">
            <a class="btn btn-primary" href="${pageContext.request.contextPath}/admin/addAdmin">新增</a>
            <a class="btn btn-primary" href="${pageContext.request.contextPath}/dish/allDish">全部</a>
        </div>
        <div class="col-md-4 column">
            <form class="form-inline" action="${pageContext.request.contextPath}/admin/queryAdmin" method="post" style="float: right">
                <span style="color:red;font-weight: bold">${error}</span>
                <input type="text" name="queryAdminName" class="form-control" placeholder="请输入查询管理员名称">
                <input type="submit" value="查询" class="btn btn-primary">

            </form>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-md-12 column">
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th>管理员编号</th>
                    <th>管理员账户</th>
                    <th>管理员密码</th>
             
                    <th>操作</th>
                </tr>
                </thead>

                <tbody>
                <c:forEach var="admin" items="${requestScope.get('list')}">
                    <tr>
                        <td>${admin.getId()}</td>
                        <td>${admin.getAccount()}</td>
                        <td>${admin.getPassword()}</td>

                        <td>
                            <a href="${pageContext.request.contextPath}/admin/toUpdateAdmin?id=${admin.getId()}">更改</a> |
                            <a href="${pageContext.request.contextPath}/admin/del/${admin.getId()}">删除</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>