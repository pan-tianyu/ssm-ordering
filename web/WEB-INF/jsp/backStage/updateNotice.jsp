<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>修改公告</title>
<style>
	.table tr{
		text-align: center;
		}	
</style>
</head>
<body>
	<form action = "${pageContext.request.contextPath}/notice/updateNotice" method="get">
			<table class="table" border="2px" cellspacing="0" style="width: 100%;">
				<input type="hidden" name="id" value="${notice.id }" />
				<tr><td>公告标题：<input type="text"name="title" value="${notice.title }"/></td></tr>
				<tr><td>公告内容：<input type="text"name="concept" value="${notice.concept }"/></td></tr>
				<tr><td>公告时间：<input type="text"name="time" value="${notice.time}"/></td></tr>
				<tr><td><button type="submit">修改</button></td></tr>
			</table>
		</form>
</body>
</html>