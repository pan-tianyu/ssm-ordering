<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<form action="${pageContext.request.contextPath}/ordering/toUpdateOrdering" method="post">
			
			<table cellspacing="0" align="center" border="2px"style="width: 100%;text-align: center">
				<tr>
					<td colspan="7" style="text-align: center;">订单信息列表</td>
				</tr>
				<tr>
					<td>订单号</td>
					<td>客户名</td>
					<td>菜品名称</td>
					<td>订购数量</td>
					<td>订购时间</td>
					<td>是否派送</td>
					<td>确认订单</td>
				</tr>
				<c:forEach items="${ordering}" >
					<tr>
						<td>${ordering.id}</td>
						<td>客户名</td>
						<td>菜品名称</td>
						<td>订购数量</td>
						<td>订购时间</td>
						<td>是否派送</td>
						<td>确认订单</td>
					</tr>
				</c:forEach>
			</table>
			
			<table border="2" align="center" cellspacing="0" cellpadding="0"style="margin-top: 200px">
				<tr>
					<td>订单号：</td>
					<td><input type="text" name="id"/></td>
				</tr>
				<tr style="text-align: center;"><td colspan="2"><button href="${ordering.id }">查询</button></td></tr>
			</table>
			
		</form>
</body>
</html>