<%--
  Created by IntelliJ IDEA.
  User: 潘天宇
  Date: 2021/12/27
  Time: 19:26
  To change this template use File | Settings | File Templates.
--%>
<%--
  Created by IntelliJ IDEA.
  User: 潘天宇
  Date: 2021/12/27
  Time: 19:18
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>菜品列表</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- 引入 Bootstrap -->
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<div class="container">

    <div class="row clearfix">
        <div class="col-md-12 column">
            <div class="page-header">
                <h1>
                    <small>菜品列表 —— 显示所有菜品</small>
                </h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 column">
            <a class="btn btn-primary" href="${pageContext.request.contextPath}/menu/toAddMenu">新增</a>
            <a class="btn btn-primary" href="${pageContext.request.contextPath}/menu/allMenu">全部</a>
        </div>
        <div class="col-md-4 column">
            <form class="form-inline" action="${pageContext.request.contextPath}/menu/queryMenu" method="post" style="float: right">
                <span style="color:red;font-weight: bold">${error}</span>
                <input type="text" name="queryMenuName" class="form-control" placeholder="请输入查询菜品名称">
                <input type="submit" value="查询" class="btn btn-primary">

            </form>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-md-12 column">
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th>菜品编号</th>
                    <th>菜品名称</th>
                    <th>菜品用料</th>
                    <th>菜品类型id</th>
                    <th>菜品价格</th>
                    <th>菜品会员价格</th>
                    <th>菜品状态</th>
                    <th>菜品图片</th>                    <th>操作</th>
                </tr>
                </thead>

                <tbody>
                <c:forEach var="menu" items="${requestScope.get('list')}">
                    <tr>
                        <td>${menu.getId()}</td>
                        <td>${menu.getMenuName()}</td>
                        <td>${menu.getMaterial()}</td>
                        <td>${menu.getTypeId()}</td>
                        <td>${menu.getPrice()}</td>
                        <td>${menu.getUnit_price()}</td>
                        <td>${menu.getState()}</td>
                        <td>${menu.getPicture()}</td>

                        <td>
                            <a href="${pageContext.request.contextPath}/menu/toUpdateMenu?id=${menu.getId()}">更改</a> |
                            <a href="${pageContext.request.contextPath}/menu/del/${menu.getId()}">删除</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
