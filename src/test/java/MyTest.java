import com.pan.pojo.Orderings;
import com.pan.service.OrderingService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MyTest {
    @Test
    public void test(){
        ApplicationContext context =new ClassPathXmlApplicationContext("applicationContext.xml");
        OrderingService orderingServiceImpl=(OrderingService)context.getBean("OrderingServiceImpl");
        for (Orderings orderings : orderingServiceImpl.queryAllOrdering()) {
            System.out.println(orderings);
            
        }
    }
}
