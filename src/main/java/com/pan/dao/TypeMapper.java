package com.pan.dao;

import com.pan.pojo.Types;
import com.pan.pojo.Types;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TypeMapper {
    //增加一个Type
    int addType(Types type);

    //根据id删除一个Type
    int deleteTypeById(int id);

    //更新Type
    int updateType(Types types);

    //根据id查询,返回一个Type
    Types queryTypeById(int id);

    //查询全部Type,返回list集合
    List<Types> queryAllType();

    Types queryTypeByName(@Param("typeName") String typeName);
}
