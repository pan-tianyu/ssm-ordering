package com.pan.dao;

import com.pan.pojo.Menus;
import com.pan.pojo.Menus;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MenuMapper {
        //增加一个Menu
        int addMenu(Menus menu);

        //根据id删除一个Menu
        int deleteMenuById(int id);

        //更新Menu
        int updateMenu(Menus menus);

        //根据id查询,返回一个Menu
        Menus queryMenuById(int id);

        //查询全部Menu,返回list集合
        List<Menus> queryAllMenu();

        List<Menus> doGetList(@Param("start") String start, @Param("pageSize") String pageSize);

        Menus queryMenuByName(@Param("menuName") String menuName);



}
