package com.pan.dao;

import com.pan.pojo.Notices;
import com.pan.pojo.Notices;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface NoticeMapper {
    int addNotice(Notices notices);

    //根据id删除一个Notice
    int deleteNoticeById(int id);

    //更新Notice
    int updateNotice(Notices notices);

    //根据id查询,返回一个Notice
    Notices queryNoticeById(int id);

    //查询全部Notice,返回list集合
    List<Notices> queryAllNotice();

    Notices queryNoticeByName(@Param("title") String title);
}
