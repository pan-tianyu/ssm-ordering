package com.pan.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class LoginInterceptor implements HandlerInterceptor {

    private static final List<String> EXCLUDE_PATH= Arrays.asList("/","/css/**","/js/**","/img/**","/media/**","/vendors/**");

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws ServletException, IOException {
        // 如果是登陆页面则放行
        System.out.println("uri: " + request.getRequestURI());

        if (request.getRequestURI().contains("login")) {
            return true;
        }
        if (request.getRequestURI().contains("jump")) {
            return true;
        }
        if (request.getRequestURI().contains("register")) {
            return true;
        }
        if (request.getRequestURI().contains("main2")) {
            return true;
        }
        if (request.getRequestURI().contains("admin")) {
            return true;
        }
        if (request.getRequestURI().contains("main")) {
            return true;
        }
        if (request.getRequestURI().contains("footer")) {
            return true;
        }
        if (request.getRequestURI().contains("header")) {
            return true;
        }
        if (request.getRequestURI().contains("home")) {
            return true;
        }
//        if (requestUri.startsWith(url)) {
//            System.out.println("放行的url");
//            return true;
//        }

        HttpSession session = request.getSession();

        // 如果用户已登陆也放行
        if(session.getAttribute("user") != null) {
            return true;
        }
        if(session.getAttribute("account") != null) {
            return true;
        }
        if(session.getAttribute("admin") != null) {
            return true;
        }
        if(session.getAttribute("admins") != null) {
            return true;
        }
        if(session.getAttribute("users") != null) {
            return true;
        }
        if(session.getAttribute("password") != null) {
            return true;
        }
        if(session.getAttribute("123") != null) {
            return true;
        }


        // 用户没有登陆跳转到登陆页面
        request.getRequestDispatcher("/WEB-INF/jsp/foreStage/login.jsp").forward(request, response);
        return false;
    }

    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
