package com.pan.service;

import com.pan.pojo.Menus;
import com.pan.pojo.Menus;

import java.util.List;

public interface MenuService {
    //增加一个Menu
    int addMenu(Menus menu);
    //根据id删除一个Menu
    int deleteMenuById(int id);
    //更新Menu
    int updateMenu(Menus menus);
    //根据id查询,返回一个Menu
    Menus queryMenuById(int id);
    //查询全部Menu,返回list集合
    List<Menus> queryAllMenu();

//    // 查询总条数
//    public int getTotcalCount();
//    //根据起始下标和每页显示数量查询数据
//    public List<Menus> getPageMenusList(int pageNo, int pagesize);
    Menus queryMenuByName(String menuName);
}
