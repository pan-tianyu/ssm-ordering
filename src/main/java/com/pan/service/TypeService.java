package com.pan.service;

import com.pan.pojo.Types;
import com.pan.pojo.Types;

import java.util.List;

public interface TypeService {
    //增加一个Type
    int addType(Types type);
    //根据id删除一个Type
    int deleteTypeById(int id);
    //更新Type
    int updateType(Types types);
    //根据id查询,返回一个Type
    Types queryTypeById(int id);
    //查询全部Type,返回list集合
    List<Types> queryAllType();
    Types queryTypeByName(String typeName);
}
