package com.pan.service;

import com.pan.pojo.Customers;

import java.util.List;

public interface CustomerService {
    //增加一个Customer
    int addCustomer(Customers customer);
    //根据id删除一个Customer
    int deleteCustomerById(int id);
    //更新Customer
    int updateCustomer(Customers customers);
    //根据id查询,返回一个Customer
    Customers queryCustomerById(int id);
    //查询全部Customer,返回list集合
    List<Customers> queryAllCustomer();

    boolean login(Customers customers);

    Customers queryCustomerByName(String name);
}
