package com.pan.service;

import com.pan.dao.CustomerMapper;
import com.pan.pojo.Customers;
import com.pan.pojo.Customers;

import java.util.List;

public class CustomerServiceImpl implements CustomerService {
    //调用dao层的操作，设置一个set接口，方便Spring管理
    private CustomerMapper customerMapper;

    public void setCustomerMapper(CustomerMapper customerMapper) {
        this.customerMapper = customerMapper;
    }

    public int addCustomer(Customers customer) {
        return customerMapper.addCustomer(customer);
    }

    public int deleteCustomerById(int id) {
        return customerMapper.deleteCustomerById(id);
    }

    public int updateCustomer(Customers customers) {
        return customerMapper.updateCustomer(customers);
    }

    public Customers queryCustomerById(int id) {
        return customerMapper.queryCustomerById(id);
    }

    public List<Customers> queryAllCustomer() {
        return customerMapper.queryAllCustomer();
    }

    @Override
    public boolean login(Customers customers) {
        System.out.println("业务层：用户登录");
        return customerMapper.login(customers);
    }
    public Customers queryCustomerByName(String name){
        return customerMapper.queryCustomerByName(name);
    }
}
