package com.pan.service;

import com.pan.dao.TypeMapper;
import com.pan.pojo.Types;
import com.pan.pojo.Types;

import java.util.List;

public class TypeServiceImpl implements TypeService{
    //调用dao层的操作，设置一个set接口，方便Spring管理
    private TypeMapper typeMapper;

    public void setTypeMapper(TypeMapper typeMapper) {
        this.typeMapper = typeMapper;
    }

    public int addType(Types type) {
        return typeMapper.addType(type);
    }

    public int deleteTypeById(int id) {
        return typeMapper.deleteTypeById(id);
    }

    public int updateType(Types types) {
        return typeMapper.updateType(types);
    }

    public Types queryTypeById(int id) {
        return typeMapper.queryTypeById(id);
    }

    public List<Types> queryAllType() {
        return typeMapper.queryAllType();
    }
    public Types queryTypeByName(String typeName){
        return typeMapper.queryTypeByName(typeName);
    }
}
