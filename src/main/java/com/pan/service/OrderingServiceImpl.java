package com.pan.service;

import com.pan.dao.OrderingMapper;
import com.pan.pojo.Orderings;

import java.util.List;

public class OrderingServiceImpl implements OrderingService{
    //调用dao层的操作，设置一个set接口，方便Spring管理
    private OrderingMapper orderingMapper;

    public void setOrderingMapper(OrderingMapper orderingMapper) {
        this.orderingMapper = orderingMapper;
    }

    public int addOrdering(Orderings ordering) {
        return orderingMapper.addOrdering(ordering);
    }

    public int deleteOrderingById(int id) {
        return orderingMapper.deleteOrderingById(id);
    }

    public int updateOrdering(Orderings orderings) {
        return orderingMapper.updateOrdering(orderings);
    }

    public Orderings queryOrderingById(int id) {
        return orderingMapper.queryOrderingById(id);
    }

    public List<Orderings> queryAllOrdering() {
        return orderingMapper.queryAllOrdering();
    }

    public Orderings queryOrderingByName(String name){

        return orderingMapper.queryOrderingByName(name);
    }
}
