package com.pan.service;

import com.pan.dao.AdminMapper;
import com.pan.pojo.Admins;

import java.util.List;

public class AdminServiceImpl implements AdminService{
    //调用dao层的操作，设置一个set接口，方便Spring管理
    private AdminMapper adminMapper;

    public void setAdminMapper(AdminMapper adminMapper) {
        this.adminMapper = adminMapper;
    }

    public int addAdmin(Admins admin) {
        return adminMapper.addAdmin(admin);
    }

    public int deleteAdminById(int id) {
        return adminMapper.deleteAdminById(id);
    }

    public int updateAdmin(Admins admins) {
        return adminMapper.updateAdmin(admins);
    }

    public Admins queryAdminById(int id) {
        return adminMapper.queryAdminById(id);
    }

    public List<Admins> queryAllAdmin() {
        return adminMapper.queryAllAdmin();
    }

    public Admins queryAdminByName(String adminName){
        return adminMapper.queryAdminByName(adminName);
    }

    @Override
    public boolean login(Admins admins) {
        System.out.println("业务层：管理员登录");
        return adminMapper.login(admins);
    }
}
