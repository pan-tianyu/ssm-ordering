package com.pan.service;

import com.pan.dao.NoticeMapper;
import com.pan.pojo.Notices;
import com.pan.pojo.Notices;

import java.util.List;

public class NoticeServiceImpl implements NoticeService{
    //调用dao层的操作，设置一个set接口，方便Spring管理
    private NoticeMapper noticeMapper;

    public void setNoticeMapper(NoticeMapper noticeMapper) {
        this.noticeMapper = noticeMapper;
    }

    public int addNotice(Notices notice) {
        return noticeMapper.addNotice(notice);
    }

    public int deleteNoticeById(int id) {
        return noticeMapper.deleteNoticeById(id);
    }

    public int updateNotice(Notices notices) {
        return noticeMapper.updateNotice(notices);
    }

    public Notices queryNoticeById(int id) {
        return noticeMapper.queryNoticeById(id);
    }

    public List<Notices> queryAllNotice() {
        return noticeMapper.queryAllNotice();
    }
    
    public Notices queryNoticeByName(String noticeName){
        return noticeMapper.queryNoticeByName(noticeName);
    }

}
