package com.pan.service;

import com.pan.pojo.Orderings;

import java.util.List;

public interface OrderingService {
    //增加一个Ordering
    int addOrdering(Orderings ordering);
    //根据id删除一个Ordering
    int deleteOrderingById(int id);
    //更新Ordering
    int updateOrdering(Orderings orderings);
    //根据id查询,返回一个Ordering
    Orderings queryOrderingById(int id);
    //查询全部Ordering,返回list集合
    List<Orderings> queryAllOrdering();

    Orderings queryOrderingByName(String name);
}
