package com.pan.service;

import com.pan.pojo.Admins;

import java.util.List;

public interface AdminService {
    //增加一个Admin
    int addAdmin(Admins admin);
    //根据id删除一个Admin
    int deleteAdminById(int id);
    //更新Admin
    int updateAdmin(Admins admins);
    //根据id查询,返回一个Admin
    Admins queryAdminById(int id);
    //查询全部Admin,返回list集合
    List<Admins> queryAllAdmin();
    boolean login(Admins admins);

    Admins queryAdminByName(String adminName);
}
