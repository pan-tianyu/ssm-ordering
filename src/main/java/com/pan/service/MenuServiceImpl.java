package com.pan.service;

import com.pan.dao.MenuMapper;
import com.pan.pojo.Menus;
import com.pan.pojo.Menus;

import java.util.List;

public class MenuServiceImpl implements MenuService{
    //调用dao层的操作，设置一个set接口，方便Spring管理
    private MenuMapper menuMapper;

    public void setMenuMapper(MenuMapper menuMapper) {
        this.menuMapper = menuMapper;
    }

    public int addMenu(Menus menu) {
        return menuMapper.addMenu(menu);
    }

    public int deleteMenuById(int id) {
        return menuMapper.deleteMenuById(id);
    }

    public int updateMenu(Menus menus) {
        return menuMapper.updateMenu(menus);
    }

    public Menus queryMenuById(int id) {
        return menuMapper.queryMenuById(id);
    }

    public List<Menus> queryAllMenu() {
        return menuMapper.queryAllMenu();
    }

    public Menus queryMenuByName(String menuName){
        return menuMapper.queryMenuByName(menuName);
    }


}
