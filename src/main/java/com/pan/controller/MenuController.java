package com.pan.controller;

import com.pan.pojo.Menus;
import com.pan.pojo.Menus;
import com.pan.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/menu")
public class MenuController {
    @Autowired
    @Qualifier("MenuServiceImpl")
    private MenuService menuService;

    @RequestMapping("/allMenu")
    public String list(Model model) {
        List<Menus> list = menuService.queryAllMenu();
        model.addAttribute("list", list);
        return "backStage/allMenu";
    }

    @RequestMapping("/toAddMenu")
    public String toAddPaper() {
        return "backStage/addMenu";
    }

    @RequestMapping("/addMenu")
    public String addPaper(Menus menus) {
        System.out.println(menus);
        menuService.addMenu(menus);
        return "redirect:/menu/allMenu";
    }
    @RequestMapping("/toUpdateMenu")
    public String toUpdateMenu(Model model, int id) {
        Menus menus = menuService.queryMenuById(id);
        System.out.println(menus);
        model.addAttribute("menu",menus );
        return "backStage/updateMenu";
    }

    @RequestMapping("/updateMenu")
    public String updateMenu(Model model, Menus menu) {
        System.out.println(menu);
        menuService.updateMenu(menu);
        Menus menus = menuService.queryMenuById(menu.getId());
        model.addAttribute("menus", menus);
        return "redirect:/menu/allMenu";
    }
    @RequestMapping("/del/{menuId}")
    public String deleteMenu(@PathVariable("menuId") int id) {
        menuService.deleteMenuById(id);
        return "redirect:/menu/allMenu";
    }
    @RequestMapping("/queryMenu")
    public String queryMenu(String queryMenuName,Model model){
        Menus menus=menuService.queryMenuByName(queryMenuName);

        List<Menus> list =new ArrayList<Menus>();
        list.add(menus);
        if(menus==null){
            list=menuService.queryAllMenu();
            model.addAttribute("error","未查到");
        }
        model.addAttribute("list", list);
        return "backStage/allMenu";

    }
}
