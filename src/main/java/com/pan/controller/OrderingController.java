package com.pan.controller;

import com.pan.pojo.Orderings;
import com.pan.pojo.Orderings;
import com.pan.service.OrderingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/ordering")
public class OrderingController {
    @Autowired
    @Qualifier("OrderingServiceImpl")
    private OrderingService orderingService;

    @RequestMapping("/allOrdering")
    public String list(Model model) {
        List<Orderings> list = orderingService.queryAllOrdering();
        model.addAttribute("list", list);
        return "backStage/allOrdering";
    }
    @RequestMapping("/myOrder")
    public String myOrder(){
//        int id,Model model
//        Orderings orderings=orderingService.queryOrderingById(1);
//        List<Orderings> list1 =new ArrayList<Orderings>();
//        list1.add(orderings);
//        model.addAttribute("list1", list1);
        return "foreStage/myOrder";

    }

    @RequestMapping("/toAddOrdering")
    public String toAddPaper(Orderings orderings) {
        orderingService.addOrdering(orderings);
        return "redirect:/foreStage/myOrder";
    }

    @RequestMapping("/addOrdering")
    public String addPaper(Orderings orderings) {
        System.out.println(orderings);
        orderingService.addOrdering(orderings);
        return "redirect:/ordering/allOrdering";
    }
    @RequestMapping("/toUpdateOrdering")
    public String toUpdateOrdering(Model model, int id) {
        Orderings orderings = orderingService.queryOrderingById(id);
        System.out.println(orderings);
        model.addAttribute("ordering",orderings );
        return "backStage/selectOrder";
    }

    @RequestMapping("/updateOrdering")
    public String updateOrdering(Model model, Orderings ordering) {
        System.out.println(ordering);
        orderingService.updateOrdering(ordering);
        Orderings orderings = orderingService.queryOrderingById(ordering.getId());
        model.addAttribute("orderings", orderings);
        return "redirect:/ordering/allOrdering";
    }
    @RequestMapping("/del/{orderingId}")
    public String deleteOrdering(@PathVariable("orderingId") int id) {
        orderingService.deleteOrderingById(id);
        return "redirect:/ordering/allOrdering";
    }
    @RequestMapping("/queryOrdering")
    public String queryOrdering(String queryOrderingName,Model model){

        Orderings orderings=orderingService.queryOrderingByName(queryOrderingName);

        List<Orderings> list =new ArrayList<Orderings>();
        list.add(orderings);
        if(orderings==null){
            list=orderingService.queryAllOrdering();
            model.addAttribute("error","未查到");
        }
        model.addAttribute("list", list);
        return "backStage/allOrdering";

    }
}
