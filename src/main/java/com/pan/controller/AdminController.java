package com.pan.controller;

import com.pan.pojo.Admins;
import com.pan.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    @Qualifier("AdminServiceImpl")
    private AdminService adminService;

    @RequestMapping("/allAdmin")
    public String list(Model model) {
        List<Admins> list = adminService.queryAllAdmin();
        model.addAttribute("list", list);
        return "backStage/allAdmin";
    }
    @RequestMapping("/toAddAdmin")
    public String toAddAdmin() {
        return "backStage/addAdmin";
    }

    @RequestMapping("/addAdmin")
    public String addPaper(Admins admins) {
        System.out.println(admins);
        adminService.addAdmin(admins);
        return "redirect:/admin/allAdmin";
    }
    @RequestMapping("/toUpdateAdmin")
    public String toUpdateAdmin(Model model, int id) {
        Admins admins = adminService.queryAdminById(id);
        System.out.println(admins);
        model.addAttribute("admin",admins );
        return "backStage/updateAdmin";
    }

    @RequestMapping("/updateAdmin")
    public String updateAdmin(Model model, Admins admin) {
        System.out.println(admin);
        adminService.updateAdmin(admin);
        Admins admins = adminService.queryAdminById(admin.getId());
        model.addAttribute("admins", admins);
        return "redirect:/admin/allAdmin";
    }
    @RequestMapping("/del/{adminId}")
    public String deleteAdmin(@PathVariable("adminId") int id) {
        adminService.deleteAdminById(id);
        return "redirect:/admin/allAdmin";
    }
    //跳转到登陆页面
    @RequestMapping("/jumpLogin")
    public String jumpLogin() throws Exception {
        return "backStage/login";
    }
    @RequestMapping("/leftMenu")
    public String leftMenu() throws Exception {
        return "backStage/leftMenu";
    }
    @RequestMapping("/topWelcome")
    public String topWelcome() throws Exception {
        return "backStage/topWelcome";
    }

    //登陆提交
    @RequestMapping("/login")
    public String login(HttpSession session, Admins admins)throws Exception {
        if(adminService.login(admins)){
//            System.out.println("接收前端==="+admins);
            session.setAttribute("admin", admins);
            return "backStage/main";
        }else{
            return "backStage/login";
        }
    }
    
    @RequestMapping("/queryAdmin")
    public String queryAdmin(String queryAdminName,Model model){
        Admins admins=adminService.queryAdminByName(queryAdminName);

        List<Admins> list =new ArrayList<Admins>();
        list.add(admins);
        if(admins==null){
            list=adminService.queryAllAdmin();
            model.addAttribute("error","未查到");
        }
        model.addAttribute("list", list);
        return "backStage/allAdmin";

    }
    //退出登陆
    @RequestMapping("logout")
    public String logout(HttpSession session) throws Exception {
        // session 过期
        session.invalidate();
        return "foreStage/login";
    }

}
