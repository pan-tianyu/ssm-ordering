package com.pan.controller;

import com.pan.pojo.Customers;
import com.pan.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/customer")
public class CustomerController {
    @Autowired
    @Qualifier("CustomerServiceImpl")
    private CustomerService customerService;

    @RequestMapping("/allCustomer")
    public String list(Model model) {
        List<Customers> list = customerService.queryAllCustomer();
        model.addAttribute("list", list);
        return "backStage/allCustomer";
    }
    @RequestMapping("/toAddCustomer")
    public String toAddPaper() {
        return "backStage/addCustomer";
    }

    @RequestMapping("/addCustomer")
    public String addPaper(Customers customers) {
        System.out.println(customers);
        customerService.addCustomer(customers);
        return "redirect:/customer/allCustomer";
    }
    @RequestMapping("/toUpdateCustomer")
    public String toUpdateCustomer(Model model, int id) {
        Customers customers = customerService.queryCustomerById(id);
        System.out.println(customers);
        model.addAttribute("customer",customers );
        return "foreStage/userCenter";
    }

    @RequestMapping("/updateCustomer")
    public String updateCustomer(Model model, Customers customer) {
        System.out.println(customer);
        customerService.updateCustomer(customer);
        Customers customers = customerService.queryCustomerById(customer.getId());
        model.addAttribute("customers", customers);
        return "redirect:/customer/allCustomer";
    }
    @RequestMapping("/del/{customerId}")
    public String deleteCustomer(@PathVariable("customerId") int id) {
        customerService.deleteCustomerById(id);
        return "redirect:/customer/allCustomer";
    }
    @RequestMapping("/login")
    public String login(HttpSession session, Customers customers)throws Exception {
        if(customerService.login(customers)){
            System.out.println("接收前端==="+customers);
            session.setAttribute("user", customers);
            return "foreStage/home";
        }else{

            return "foreStage/login";
        }
    }
    @RequestMapping("/queryCustomer")
    public String queryCustomer(String queryCustomerName,Model model){
        Customers customers=customerService.queryCustomerByName(queryCustomerName);

        List<Customers> list =new ArrayList<Customers>();
        list.add(customers);
        if(customers==null){
            list=customerService.queryAllCustomer();
            model.addAttribute("error","未查到");
        }
        model.addAttribute("list", list);
        return "backStage/allCustomer";

    }
    //退出登陆
    @RequestMapping("logout")
    public String logout(HttpSession session) throws Exception {
        // session 过期
        session.invalidate();
        return "foreStage/login";
    }
}
