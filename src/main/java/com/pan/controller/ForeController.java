package com.pan.controller;
import com.pan.pojo.Menus;
import com.pan.pojo.Notices;
import com.pan.service.MenuService;
import com.pan.service.NoticeService;
import com.pan.service.OrderingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/fore")
public class ForeController {

        @Autowired
        @Qualifier("MenuServiceImpl")
        private MenuService menuService;
        @Qualifier("NoticeServiceImpl")
        private final NoticeService noticeService;
        @Qualifier("OrderingServiceImpl")
        private OrderingService orderingService;

        public ForeController(NoticeService noticeService) {
                this.noticeService = noticeService;
        }

        @RequestMapping("/home")
        public String list(Model model,String start, String pageSize) {
                List<Menus> list = menuService.queryAllMenu();

                model.addAttribute("list", list);
                return "foreStage/home";
//                return MenuMapper.doGetList(start,pageSize);
//        public String home() throws Exception {
//                return "foreStage/home";
        }
        public String list(Model model){
         List<Notices> list1 = noticeService.queryAllNotice();
                model.addAttribute("list1", list1);
                return "foreStage/home";

        }


        @RequestMapping("/aboutUs")
        public String aboutUs() throws Exception {
                return "foreStage/aboutUs";
        }
        @RequestMapping("/article")
        public String article() throws Exception {
                return "foreStage/article";
        }
        @RequestMapping("/buffetCar")
        public String buffetCar() throws Exception {
                return "foreStage/buffetCar";
        }
        @RequestMapping("/delivery")
        public String delivery() throws Exception {
                return "foreStage/delivery";
        }
        @RequestMapping("/detailOfDishes")
        public String detailOfDishes() throws Exception {
                return "foreStage/detailOfDishes";
        }

        @RequestMapping("/notice")
        public String notice() throws Exception {
                return "foreStage/notice";
        }
        @RequestMapping("/userCenter")
        public String userCenter() throws Exception {
                return "foreStage/userCenter";
        }
        @RequestMapping("/footer")
        public String footer() throws Exception {
                return "foreStage/footer";
        }
        @RequestMapping("/header")
        public String header() throws Exception {
                return "foreStage/header";
        }
        @RequestMapping("/main2")
        public String main() throws Exception {
                return "foreStage/main2";
        }





}




