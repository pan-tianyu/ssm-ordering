package com.pan.controller;

import com.pan.pojo.Notices;
import com.pan.pojo.Notices;
import com.pan.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/notice")
public class NoticeController {
    @Autowired
    @Qualifier("NoticeServiceImpl")
    private NoticeService noticeService;

    @RequestMapping("/allNotice")
    public String list(Model model) {
        List<Notices> list = noticeService.queryAllNotice();
        model.addAttribute("list", list);
        return "backStage/allNotice";

    }
    @RequestMapping("/toAddNotice")
    public String toAddPaper() {
        return "backStage/addNotice";
    }

    @RequestMapping("/addNotice")
    public String addPaper(Notices notices) {
        System.out.println(notices);
        noticeService.addNotice(notices);
        return "redirect:/notice/allNotice";
    }
    @RequestMapping("/toUpdateNotice")
    public String toUpdateNotice(Model model, int id) {
        Notices notices = noticeService.queryNoticeById(id);
        System.out.println(notices);
        model.addAttribute("notice",notices );
        return "backStage/updateNotice";
    }

    @RequestMapping("/updateNotice")
    public String updateNotice(Model model, Notices notice) {
        System.out.println(notice);
        noticeService.updateNotice(notice);
        Notices notices = noticeService.queryNoticeById(notice.getId());
        model.addAttribute("notices", notices);
        return "redirect:/notice/allNotice";
    }
    @RequestMapping("/del/{noticeId}")
    public String deleteNotice(@PathVariable("noticeId") int id) {
        noticeService.deleteNoticeById(id);
        return "redirect:/notice/allNotice";
    }
    @RequestMapping("/queryNotice")
    public String queryNotice(String queryNoticeName,Model model){
        Notices notices=noticeService.queryNoticeByName(queryNoticeName);

        List<Notices> list =new ArrayList<Notices>();
        list.add(notices);
        if(notices==null){
            list=noticeService.queryAllNotice();
            model.addAttribute("error","未查到");
        }
        model.addAttribute("list", list);
        return "backStage/allNotice";

    }
}
