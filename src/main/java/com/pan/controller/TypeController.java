package com.pan.controller;

import com.pan.pojo.Types;
import com.pan.pojo.Types;
import com.pan.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/type")
public class TypeController {
    @Autowired
    @Qualifier("TypeServiceImpl")
    private TypeService typeService;

    @RequestMapping("/allType")
    public String list(Model model) {
        List<Types> list = typeService.queryAllType();
        model.addAttribute("list", list);
        return "backStage/allType";
    }
    @RequestMapping("/toAddType")
    public String toAddPaper() {
        return "backStage/addType";
    }

    @RequestMapping("/addType")
    public String addPaper(Types types) {
        System.out.println(types);
        typeService.addType(types);
        return "redirect:/type/allType";
    }
    @RequestMapping("/toUpdateType")
    public String toUpdateType(Model model, int id) {
        Types types = typeService.queryTypeById(id);
        System.out.println(types);
        model.addAttribute("type",types );
        return "backStage/updateType";
    }

    @RequestMapping("/updateType")
    public String updateType(Model model, Types type) {
        System.out.println(type);
        typeService.updateType(type);
        Types types = typeService.queryTypeById(type.getId());
        model.addAttribute("types", types);
        return "redirect:/type/allType";
    }
    @RequestMapping("/del/{typeId}")
    public String deleteType(@PathVariable("typeId") int id) {
        typeService.deleteTypeById(id);
        return "redirect:/type/allType";
    }
    @RequestMapping("/queryType")
    public String queryType(String queryTypeName,Model model){
        Types types=typeService.queryTypeByName(queryTypeName);

        List<Types> list =new ArrayList<Types>();
        list.add(types);
        if(types==null){
            list=typeService.queryAllType();
            model.addAttribute("error","未查到");
        }
        model.addAttribute("list", list);
        return "backStage/allType";

    }
}
