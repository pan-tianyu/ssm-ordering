package com.pan.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class Orderings {
    private Integer id;
    private String customer;
    private int menu_id;
    private String name;
    private int quantity;
    private String time;
    private String send_or_not;
    private String confirm;

}
