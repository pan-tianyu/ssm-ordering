package com.pan.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class Notices {
    private int id;
    private String title;
    private String concept;
    private String time;

}
