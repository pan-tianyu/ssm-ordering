package com.pan.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class Menus {
    private int id;
    private String menuName;
    private String material;
    private int typeId;
    private float price;
    private float unit_price;
    private String state;
    private String picture;

}
