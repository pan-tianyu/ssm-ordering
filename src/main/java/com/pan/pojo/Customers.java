package com.pan.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Customers {
    private int id;
    private String account;
    private String password;
    private String name;
    private String gender;
    private String birthday;
    private String idcard;
    private String address;
    private String telephone;
    private String email;
    private String code;
}
