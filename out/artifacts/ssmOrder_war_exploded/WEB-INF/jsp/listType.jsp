<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<table border="1" cellspacing="0" align="center" style="width: 100%">
		<tr>
			<td>序号</td>
			<td>类别名称</td>
			<td>操作</td>
		</tr>
		<c:forEach items="${list }" var="t">
			<tr>
				<td>${t.id }</td>
				<td>${t.typename }</td>
				<td><a href="TypeUpdate?id=${t.id }" style="color: darkorange;"style="text-decoration: none;">修改</a> <a
					href="TypeDelete?id=${t.id }" style="color: darkorange;"style="text-decoration: none;">删除</a></td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>