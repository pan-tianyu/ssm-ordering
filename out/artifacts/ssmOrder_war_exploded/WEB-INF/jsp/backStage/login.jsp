<%--
  Created by IntelliJ IDEA.
  User: 潘天宇
  Date: 2021/12/26
  Time: 15:30
  To change this template use File | Settings | File Templates.
--%>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%--<html>--%>
<%--<head>--%>
<%--  <title>ajax</title>--%>
<%--  <script src="${pageContext.request.contextPath}/statics/js/jquery-3.1.1.min.js"></script>--%>
<%--  <script>--%>

<%--    function a1(){--%>
<%--      $.post({--%>
<%--        url:"${pageContext.request.contextPath}/a3",--%>
<%--        data:{'name':$("#name").val()},--%>
<%--        success:function (data) {--%>
<%--          if (data.toString()=='OK'){--%>
<%--            $("#userInfo").css("color","green");--%>
<%--          }else {--%>
<%--            $("#userInfo").css("color","red");--%>
<%--          }--%>
<%--          $("#userInfo").html(data);--%>
<%--        }--%>
<%--      });--%>
<%--    }--%>
<%--    function a2(){--%>
<%--      $.post({--%>
<%--        url:"${pageContext.request.contextPath}/a3",--%>
<%--        data:{'password':$("#password").val()},--%>
<%--        success:function (data) {--%>
<%--          if (data.toString()=='OK'){--%>
<%--            $("#passwordInfo").css("color","green");--%>
<%--          }else {--%>
<%--            $("#passwordInfo").css("color","red");--%>
<%--          }--%>
<%--          $("#passwordInfo").html(data);--%>
<%--        }--%>
<%--      });--%>
<%--    }--%>

<%--  </script>--%>
<%--</head>--%>
<%--<body>--%>
<%--<p>--%>
<%--  用户名:<input type="text" id="name" onblur="a1()"/>--%>
<%--  <span id="userInfo"></span>--%>
<%--</p>--%>
<%--<p>--%>
<%--  密码:<input type="text" id="password" onblur="a2()"/>--%>
<%--  <span id="passwordInfo"></span>--%>
<%--</p>--%>
<%--</body>--%>
<%--</html>--%>



<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%--<html>--%>
<%--<head>--%>
<%--  <meta charset="UTF-8">--%>
<%--  <title>登录</title>--%>
<%--</head>--%>
<%--<body>--%>
<%--&lt;%&ndash;<form style="margin-top: 200px;margin-left: 500px;"action = "/Ordering/Back/LoginController" method="get" name="login">&ndash;%&gt;--%>
<%--<form action="${pageContext.request.contextPath}/user/login">--%>
<%--  <input type="hidden" name="action" value="login">--%>
<%--  <table border="2"style="text-align: center;"cellspacing="0">--%>
<%--    <tr>--%>
<%--      <td>账号</td>--%>
<%--      <td><input type="text" name="account"/></td>--%>
<%--    </tr>--%>
<%--    <tr>--%>
<%--      <td>密码</td>--%>
<%--      <td><input type="text" name="password"/></td>--%>
<%--    </tr>--%>
<%--    <tr><td colspan="2"><button>登录</button></td></tr>--%>
<%--  </table>--%>
<%--</form>--%>
<%--</body>--%>
<%--</html>--%>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>Login</title>

  <!--用百度的静态资源库的cdn安装bootstrap环境-->
  <!-- Bootstrap 核心 CSS 文件 -->
  <link href="http://apps.bdimg.com/libs/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
  <!--font-awesome 核心我CSS 文件-->
  <link href="//cdn.bootcss.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">

  <link href="<%=request.getContextPath()%>/img/mini.png" rel="stylesheet" type="text/img"/>

  <!-- 在bootstrap.min.js 之前引入 -->
  <script src="http://apps.bdimg.com/libs/jquery/2.0.0/jquery.min.js"></script>
  <!-- Bootstrap 核心 JavaScript 文件 -->
  <script src="http://apps.bdimg.com/libs/bootstrap/3.3.0/js/bootstrap.min.js"></script>


  <!--为了保险起见，导入了本地的jquery和fontawesome    -->
<%--  <!--jquery-->--%>
<%--  <script type="text/javascript" src="js/jquery.validate.min.js" ></script>--%>
<%--  <link rel="stylesheet" href="css/Login.css">--%>
<%--  <!--font-awesome    -->--%>
<%--  <link rel="stylesheet" href="font-awesome-4\7\0\css\font-awesome.min.css">--%>

</head>
<style>
  /*设置背景图片*/
  body {
    background-image: url(<%=request.getContextPath()%>/img/mini.png);
    background-size:cover;
    font-size: 15px;
  }
  /*给表单的边框设置大小,颜色和不透明度*/
  .form {
    background: rgba(255,255,255,0.9);
    width:400px;margin:50px auto;
  }
  /*设置字体和大小*/
  label {
    font-family: Arial, serif;font-size: 15px
  }
  /*当输入正确时,给输入框设置浅绿色*/
  input.form-control:valid{
    background-color: mintcream;
  }
  /*当输入正确时,给输入框设置浅红色*/
  input.form-control:invalid{
    background-color: #ffeeed;
  }
  /*设置图标为行内元素，并设置大小和颜色*/
  .fa{
    display: inline-block;
    top: 27px;
    left: 6px;
    position: relative;
    color: #575757;
  }
  /*为几个输入框设置padding,防止和图标重合*/
  input[type="text"],input[type="password"],input[type="email"]{
    padding-left:26px;
  }
  .form-title{
    padding-top:20px;
  }
  #button{
    padding-bottom: 50px;}
</style>
<body  >
<form action="${pageContext.request.contextPath}/admin/login">

<div class="container">
  <div class="form row">
    <form class="form-horizontal col-sm-offset-3 col-md-offset-3">
      <h3 class="form-title" align="center">登录</h3>
      <div class="col-sm-9 col-md-9">
        <div class="form-group">
          <!--        用户名图标和用户名输入框            -->
          <i class="fa fa-user" aria-hidden="true"></i>
          <input class="form-control" align="center" type="text" name="account" id="account" placeholder="请输入管理员账号" required autofocus>
        </div>
        <!--        密码图标和密码输入框                -->
        <div class="form-group">
          <i class="fa fa-key" aria-hidden="true"></i>
          <input class="form-control " align="center" type="password" name="password" id="password" placeholder="请输入密码" required>
        </div>
        <!--        用超链接跳转到注册页面               -->
        <div class="form-group">
          <br>
<%--          <a href="">没有账号？立即注册</a>--%>
        </div>
        <!--         登录按钮           -->
        <div class="form-group">
          <input type="submit" value="登录" class="btn btn-success pull-right">
        </div>

      </div>
    </form>
  </div>
</div>
</form>
</body>
</html>