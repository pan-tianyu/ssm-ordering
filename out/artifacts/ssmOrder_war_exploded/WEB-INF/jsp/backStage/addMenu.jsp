<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<form action="${pageContext.request.contextPath}/menu/addMenu" method="post">
			<table border="1" cellspacing="0" style="border-radius: 4px;width: 100%">
			<input type="hidden" name="option" value="insert"/>
				<tr><td height="15px" colspan="2"style="background-image: url(<%=request.getContextPath()%>/img/t2bg5.gif);text-align: center;" >添加菜品</td></tr>
				<tr>
					<td>菜品名称:</td>
					<td><input type="text" name="menuName"/></td>
				</tr>
				<tr>
					<td>原料:</td>
					<td><input type="text" name="material"/></td>
				</tr>
				<tr>
					<td>市场价格:</td>
					<td><input type="text" name="price"/></td>
				</tr>
				<tr>
					<td>会员价格:</td>
					<td><input type="text" name="unit_price"/></td>
				</tr>
				<tr>
					<td>说明:</td>
					<td><textarea cols="30" rows="5" name="state"></textarea></td>
				</tr>
				<tr>
					<td>菜品类别</td>
					<td><input type="text" name="typeId"/></td>
<%--					<td>--%>
<%--						<select name="typeid">--%>
<%--							<c:forEach items="${sessionScope.type }" var="t">--%>
<%--								<option value="${type.id }">${type.typename }</option>--%>
<%--							</c:forEach>--%>
<%--						</select>--%>
<%--					</td>--%>
				</tr>
				<tr>
					<td>上传图片：</td>
					<td><input type="file" name="picture"></td>
				</tr>
				<tr>
					<td colspan="2" style="text-align: center;">
						<button type="submit" >添加</button>
					</td>
				</tr>
			</table>
		</form>
</body>
</html>