<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>菜单修改</title>
</head>
<body>
	<form action = "${pageContext.request.contextPath}/menu/updateMenu">
		<table class="table" border="2px" cellspacing="0" style="width: 100%;">
			<input type="hidden" name="id" value="${menu.id }" />
			<tr><td>菜品名称：<input type="text"name="menuName" value="${menu.menuName }"/></td></tr>
			<tr><td>原料：<input type="text"name="material" value="${menu.material }"/></td></tr>
			<tr><td>菜品类别：<input type="text"name="typeId" value="${menu.typeId}"/></td></tr>
			<tr><td>单价：<input type="text"name="price" value="${menu.price }"/></td></tr>				
			<tr><td>会员单价：<input type="text"name="unit_price" value="${menu.unit_price }"/></td></tr>
			<tr><td>说明：<input type="text"name="state" value="${menu.state}"/></td></tr>
			<tr><td>图片：<input type="text"name="picture" value="${menu.picture }"/></td></tr>				
	
			<tr><td><button type="submit">修改</button></td></tr>
		</table>
	</form>
	
</body>
</html>