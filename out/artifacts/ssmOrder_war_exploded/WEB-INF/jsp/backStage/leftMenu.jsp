<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
         <style>
			a { color: #000; text-decoration: none; }
			a:hover { color: darkorange; }
			#menu { width: 200px; border: 1px solid #CCC; border-bottom:none;}
			#menu ul { list-style: none; margin: 0px; ptoAdding: 0px; }
			#menu ul li { color: #fff;  border: 1px;}
			#menu ul li ul { display:none;   top: 0px; width:200px; border:1px solid #ccc; border-bottom:none; }
			#menu ul li ul li{background-color: #FFFFFF;border: 1px solid black;}
			#menu ul li:hover{background-color: #FFFFFF;}
			#menu ul li:hover ul { display:block;}
			#menu ul  li ul {
				display: none;
			}

		 </style>
	</head>
	<body bgcolor="#7fffd4">
		<div id="menu" align="center" style="margin: auto;">
			<ul>
				<li><a>菜单管理</a>
				<ul>
					<li><a href="${pageContext.request.contextPath}/menu/toAddMenu" target="main" style="font-size: 10px;">添加菜单</a></li>
					<!-- <li><a href="menu/toAddMenu.jsp" target="main" style="font-size: 10px;"></a></li> -->
					<li><a href="${pageContext.request.contextPath}/menu/allMenu" target="main"; style="font-size: 10px;">菜单信息列表</a></li>
				</ul>   
				</li>
				<li><a>菜单类别管理</a>
						<ul>
							<li><a href="${pageContext.request.contextPath}/type/toAddType" target="main" style="font-size: 10px;">添加新类别</a></li>
							<li><a href="${pageContext.request.contextPath}/type/allType" target="main"; style="font-size: 10px;">类别信息列表</a></li>
						</ul>
				</li>
				<li><a>公告信息管理</a>
						<ul>
							<li><a href="${pageContext.request.contextPath}/notice/toAddNotice" target="main" style="font-size: 10px;">添加新通告</a></li>
							<li><a href="${pageContext.request.contextPath}/notice/allNotice" target="main" style="font-size: 10px;">通告信息列表</a></li>
						</ul>
				</li>
				<li><a>销售订单管理</a>
						<ul>
							<li><a href="${pageContext.request.contextPath}/ordering/allOrdering" target="main" style="font-size: 10px;">销售订单信息列表</a></li>
							<li><a href="${pageContext.request.contextPath}/ordering/allOrdering" target="main" style="font-size: 10px;">销售订单查询</a></li>
							<li><a href="${pageContext.request.contextPath}/ordering/allOrdering" target="main" style="font-size: 10px;">本日销售额统计</a></li>
						</ul>
				</li>
				<li><a>系统用户管理</a>
					    <ul>
							<li><a href="${pageContext.request.contextPath}/admin/toAddAdmin" target="main" style="font-size: 10px;">添加用户</a></li>
							<li><a href="${pageContext.request.contextPath}/admin/allAdmin" target="main"; style="font-size: 10px;">用户信息列表</a></li>
						</ul>
				</li>
					<li>
						<a href="${pageContext.request.contextPath}/admin/logout" target="login.html">退出</a></li>
			</ul>
		</div>
     </body>
</html>
