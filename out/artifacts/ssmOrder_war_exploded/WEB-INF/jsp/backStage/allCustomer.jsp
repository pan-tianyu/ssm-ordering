<%--
  Created by IntelliJ IDEA.
  User: 潘天宇
  Date: 2021/12/27
  Time: 19:18
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>客户列表</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- 引入 Bootstrap -->
  <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<div class="container">

  <div class="row clearfix">
    <div class="col-md-12 column">
      <div class="page-header">
        <h1>
          <small>客户列表 —— 显示所有客户</small>
        </h1>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-4 column">
      <a class="btn btn-primary" href="${pageContext.request.contextPath}/customer/addCustomer">新增</a>
      <a class="btn btn-primary" href="${pageContext.request.contextPath}/customer/allCustomer">全部</a>

    </div>
    <div class="col-md-4 column">
      <form class="form-inline" action="${pageContext.request.contextPath}/customer/queryCustomer" method="post" style="float: right">
        <span style="color:red;font-weight: bold">${error}</span>
        <input type="text" name="queryCustomerName" class="form-control" placeholder="请输入查询客户名称">
        <input type="submit" value="查询" class="btn btn-primary">

      </form>
    </div>
  </div>

  <div class="row clearfix">
    <div class="col-md-12 column">
      <table class="table table-hover table-striped">
        <thead>
        <tr>
          <th>客户编号</th>
          <th>客户账户</th>
          <th>客户密码</th>
          <th>客户姓名</th>
          <th>客户性别</th>
          <th>客户生日</th>
          <th>客户id卡</th>
          <th>客户地址</th>
          <th>客户电话</th>
          <th>客户邮箱</th>
          <th>客户code</th>
          <th>操作</th>
        </tr>
        </thead>

        <tbody>
        <c:forEach var="customer" items="${requestScope.get('list')}">
          <tr>
            <td>${customer.getId()}</td>
            <td>${customer.getAccount()}</td>
            <td>${customer.getPassword()}</td>
            <td>${customer.getName()}</td>
            <td>${customer.getGender()}</td>
            <td>${customer.getBirthday()}</td>
            <td>${customer.getIdcard()}</td>
            <td>${customer.getAddress()}</td>
            <td>${customer.getTelephone()}</td>
            <td>${customer.getEmail()}</td>
            <td>${customer.getCode()}</td>
            <td>
              <a href="${pageContext.request.contextPath}/customer/toUpdateCustomer?id=${customer.getId()}">更改</a> |
              <a href="${pageContext.request.contextPath}/customer/del/${customer.getId()}">删除</a>
            </td>
          </tr>
        </c:forEach>
        </tbody>
      </table>
    </div>
  </div>
</div>
</body>
</html>