<%--
  Created by IntelliJ IDEA.
  User: 潘天宇
  Date: 2021/12/26
  Time: 15:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Insert title here</title>
</head>
<body>
<form action="${pageContext.request.contextPath}/customer/addCustomer" method="post">
  <input type="hidden" name="option" value="insert"/>
  <table class="table" border="2px" cellspacing="0" style="width: 100%;text-align: center;">

    <tr ><td>账号：<input type="text"name="account"/></td></tr>
    <tr ><td>密码：<input type="text"name="password"/></td></tr>
    <tr ><td><button type="submit">添加</button></td></tr>
  </table>
</form>
</body>
</html>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%--<html>--%>
<%--<head>--%>
<%--  <title>Title</title>--%>
<%--</head>--%>
<%--<body>--%>
<%--<form action="${pageContext.request.contextPath}/user/register">--%>
<%--<input type="button" id="btn" value="获取数据"/>--%>
<%--<table width="80%" align="center">--%>
<%--  <tr>--%>
<%--    <td>姓名</td>--%>
<%--    <td>年龄</td>--%>
<%--    <td>性别</td>--%>
<%--  </tr>--%>
<%--  <tbody id="content">--%>
<%--  </tbody>--%>
<%--</table>--%>

<%--&lt;%&ndash;<script src="${pageContext.request.contextPath}/statics/js/jquery-3.1.1.min.js"></script>&ndash;%&gt;--%>
<%--<script>--%>

<%--  $(function () {--%>
<%--    $("#btn").click(function () {--%>
<%--      $.post("${pageContext.request.contextPath}/a2",function (data) {--%>
<%--        console.log(data)--%>
<%--        var html="";--%>
<%--        for (var i = 0; i <data.length ; i++) {--%>
<%--          html+= "<tr>" +--%>
<%--                  "<td>" + data[i].name + "</td>" +--%>
<%--                  "<td>" + data[i].age + "</td>" +--%>
<%--                  "<td>" + data[i].sex + "</td>" +--%>
<%--                  "</tr>"--%>
<%--        }--%>
<%--        $("#content").html(html);--%>
<%--      });--%>
<%--    })--%>
<%--  })--%>
<%--</script>--%>
<%--</form>--%>
<%--</body>--%>
<%--</html>--%>


<%--<!DOCTYPE html>--%>
<%--<html lang="en">--%>
<%--<head>--%>
<%--  <meta charset="UTF-8">--%>
<%--  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">--%>
<%--  <title>Register</title>--%>

<%--  <!--用百度的静态资源库的cdn安装bootstrap环境-->--%>
<%--  <!-- Bootstrap 核心 CSS 文件 -->--%>
<%--  <link href="http://apps.bdimg.com/libs/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">--%>
<%--  <!--font-awesome 核心我CSS 文件-->--%>
<%--  <link href="//cdn.bootcss.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">--%>
<%--  <!-- 在bootstrap.min.js 之前引入 -->--%>
<%--  <script src="http://apps.bdimg.com/libs/jquery/2.0.0/jquery.min.js"></script>--%>
<%--  <!-- Bootstrap 核心 JavaScript 文件 -->--%>
<%--  <script src="http://apps.bdimg.com/libs/bootstrap/3.3.0/js/bootstrap.min.js"></script>--%>


<%--  <!--为了保险起见，导入了本地的jquery和fontawesome    -->--%>
<%--  <!--jquery-->--%>
<%--  <script type="text/javascript" src="js/jquery.validate.min.js" ></script>--%>
<%--  <link rel="stylesheet" href="css/Login.css">--%>
<%--  <!--font-awesome    -->--%>
<%--  <link rel="stylesheet" href="font-awesome-4\7\0\css\font-awesome.min.css">--%>

<%--</head>--%>
<%--<body>--%>
<%--<form action="${pageContext.request.contextPath}/user/register">--%>
<%--<div class="container">--%>
<%--  <div class="form row">--%>
<%--    <form class="form-horizontal col-sm-offset col-md-offset-3">--%>
<%--      <h3 class="form-title">注册</h3>--%>
<%--      <div class="col-sm-9 col-md-9">--%>
<%--        <div class="form-group">--%>
<%--          <!--        用户名图标和用户名输入框            -->--%>
<%--          <i class="fa fa-user" aria-hidden="true"></i>--%>
<%--          <input class="form-control required" type="text" name="username" id="username" placeholder="请输入用户名" required>--%>
<%--        </div>--%>

<%--        <!--        密码图标和密码输入框                -->--%>
<%--        <div class="form-group">--%>
<%--          <i class="fa fa-key" aria-hidden="true"></i>--%>
<%--          <input class="form-control required" type="password" name="password" id="password" placeholder="请输入密码" required>--%>
<%--        </div>--%>
<%--        <!--          确认密码              -->--%>
<%--        <div class="form-group">--%>
<%--          <i class="fa fa-check-circle-o" aria-hidden="true"></i>--%>
<%--          <input class="form-control required" type="password" name="resetpw" id="resetpw" placeholder="请确认密码" required>--%>
<%--        </div>--%>
<%--        <!--        邮箱                -->--%>
<%--        <div class="form-group">--%>
<%--          <i class="fa fa-envelope" aria-hidden="true"></i>--%>
<%--          <input class="form-control required" type="email" name="email" id="email" placeholder="请输入邮箱" required>--%>
<%--        </div>--%>

<%--        <div class="form-group">--%>
<%--        </div>--%>

<%--        <!--注册按钮-->--%>
<%--        <div class="form-group">--%>
<%--          <input type="submit" value="注册" class="btn btn-success pull-right">--%>
<%--          <input type="button" οnclick="javascrtpt:window.location.href='./register'"class="btn btn-info pull-left" id="back_btn" value="返回"/>--%>
<%--        </div>--%>


<%--      </div>--%>
<%--    </form>--%>
<%--  </div>--%>
<%--</div>--%>
<%--</form>--%>
<%--</body>--%>
<%--</html>--%>