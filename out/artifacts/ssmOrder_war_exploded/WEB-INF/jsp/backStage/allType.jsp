<%--
  Created by IntelliJ IDEA.
  User: 潘天宇
  Date: 2021/12/27
  Time: 19:50
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>状态列表</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- 引入 Bootstrap -->
  <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<div class="container">

  <div class="row clearfix">
    <div class="col-md-12 column">
      <div class="page-header">
        <h1>
          <small>状态列表 —— 显示所有状态</small>
        </h1>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-4 column">
      <a class="btn btn-primary" href="${pageContext.request.contextPath}/type/toAddType">新增</a>
      <a class="btn btn-primary" href="${pageContext.request.contextPath}/type/allType">全部</a>
    </div>
    <div class="col-md-4 column">
      <form class="form-inline" action="${pageContext.request.contextPath}/type/queryType" method="post" style="float: right">
        <span style="color:red;font-weight: bold">${error}</span>
        <input type="text" name="queryTypeName" class="form-control" placeholder="请输入查询菜品名称">
        <input type="submit" value="查询" class="btn btn-primary">

      </form>
    </div>
  </div>

  <div class="row clearfix">
    <div class="col-md-12 column">
      <table class="table table-hover table-striped">
        <thead>
        <tr>
          <th>状态编号</th>
          <th>状态名称</th>


          <th>操作</th>
        </tr>
        </thead>

        <tbody>
        <c:forEach var="type" items="${requestScope.get('list')}">
          <tr>
            <td>${type.getId()}</td>
            <td>${type.getTypename()}</td>

            <td>
              <a href="${pageContext.request.contextPath}/type/toUpdateType?id=${type.getId()}">更改</a> |
              <a href="${pageContext.request.contextPath}/type/del/${type.getId()}">删除</a>
            </td>
          </tr>
        </c:forEach>
        </tbody>
      </table>
    </div>
  </div>
</div>
</body>
</html>
