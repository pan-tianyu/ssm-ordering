<%--
  Created by IntelliJ IDEA.
  User: 潘天宇
  Date: 2021/12/27
  Time: 19:39
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>订单列表</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- 引入 Bootstrap -->
  <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<div class="container">

  <div class="row clearfix">
    <div class="col-md-12 column">
      <div class="page-header">
        <h1>
          <small>订单列表 —— 显示所有订单</small>
        </h1>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-4 column">
      <a class="btn btn-primary" href="${pageContext.request.contextPath}/ordering/toAddOrdering">新增</a>
      <a class="btn btn-primary" href="${pageContext.request.contextPath}/ordering/allOrdering">全部</a>
    </div>
    <div class="col-md-4 column">
      <form class="form-inline" action="${pageContext.request.contextPath}/ordering/queryOrdering" method="post" style="float: right">
        <span style="color:red;font-weight: bold">${error}</span>
        <input type="text" name="queryOrderingName" class="form-control" placeholder="请输入查询菜品名称">
        <input type="submit" value="查询" class="btn btn-primary">

      </form>
    </div>
  </div>

  <div class="row clearfix">
    <div class="col-md-12 column">
      <table class="table table-hover table-striped">
        <thead>
        <tr>
          <th>订单编号</th>
          <th>客户</th>
          <th>菜品编号</th>
          <th>订单姓名</th>
          <th>订单密码</th>
          <th>订单数量</th>
          <th>订单时间</th>
          <th>派送</th>
          <th>确认</th>

          <th>操作</th>
        </tr>
        </thead>

        <tbody>
        <c:forEach var="ordering" items="${requestScope.get('list')}">
          <tr>
            <td>${ordering.getId()}</td>
            <td>${ordering.getCustomer()}</td>
            <td>${ordering.getMenu_id()}</td>
            <td>${ordering.getName()}</td>
            <td>${ordering.getQuantity()}</td>
            <td>${ordering.getTime()}</td>
            <td>${ordering.getSend_or_not()}</td>
            <td>${ordering.getConfirm()}</td>
            <td>
              <a href="${pageContext.request.contextPath}/ordering/toUpdateOrdering?id=${ordering.getId()}">更改</a> |
              <a href="${pageContext.request.contextPath}/ordering/del/${ordering.getId()}">删除</a>
            </td>
          </tr>
        </c:forEach>
        </tbody>
      </table>
    </div>
  </div>
</div>
</body>
</html>
