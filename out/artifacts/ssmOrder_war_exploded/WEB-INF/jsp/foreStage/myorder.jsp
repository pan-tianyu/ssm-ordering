<%--<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
		<style type="text/css">
			a{
				text-decoration: none;
				margin-left: 33px;
			}
			a:hover{
				color:black;
			}
		</style>
	</head>
	<body>
<%--	<form action="${pageContext.request.contextPath}/ordering/addOrdering" method="post">--%>
		<form>
			<table border="1px" cellspacing="0" cellpadding="0" style="list-style:none;height: 100px; width:400px;margin: auto;border: 1px #FF0000 solid;border-radius: 4px;">
				<tr>
					<td>按菜品名称查询</td>
					<td><input name="name" type="text"></td>
				</tr>
				<tr>
					<td>按销售日期查询</td>
					<td><input name="date" type="date"><button>订单查询</button></td>
				</tr>
				<tr>
					<td colspan="2">
						<a href="" style="color: #00FFFF;">我的所有订单</a>
						<a href="" style="color: lightskyblue;">未派送订单</a>
						<a href="" style="color: coral;">已派送订单</a>
					</td>
				</tr>
			</table>
			<table border="1px" cellspacing="0" cellpadding="0" style="list-style:none;width:700px;margin: auto; margin-top:15px;border: 1px #FF0000 solid;border-radius: 4px;">
				<tr>
					<td colspan="9" style="text-align: center;">订单结果结果查询列表信息</td>
				</tr>
				<tr>
					<td>菜品名称</td>
					<td>真实姓名</td>
					<td>订购电话</td>
					<td>派送地址</td>
					<td>订购数量</td>
					<td>单价(元)</td>
					<td>订购时间</td>
					<td>是否派送</td>
				</tr>
				<tr>
					<td>火锅</td>
					<td>123</td>
					<td>123</td>
					<td>123</td>
					<td>1</td>
					<td>123</td>
					<td>20211230</td>
					<td>1</td>
				</tr>

<%--				<c:forEach var="ordering" items="${requestScope.get('list2')}">--%>
<%--					<tr>--%>
<%--						<td>${ordering.getName()}</td>--%>
<%--						<td>${ordering.getCustomer()}</td>--%>
<%--						<td>${ordering.getMenu_id()}</td>--%>
<%--						<td>${ordering.getName()}</td>--%>
<%--						<td>${ordering.getQuantity()}</td>--%>
<%--						<td>${ordering.getTime()}</td>--%>
<%--						<td>${ordering.getSend_or_not()}</td>--%>
<%--						<td>${ordering.getConfirm()}</td>--%>
<%--						<td>--%>
<%--							<a href="${pageContext.request.contextPath}/ordering/updateOrdering?id=${ordering.getId()}">更改</a> |--%>
<%--							<a href="${pageContext.request.contextPath}/ordering/del/${ordering.getId()}">删除</a>--%>
<%--						</td>--%>
<%--					</tr>--%>
<%--				</c:forEach>--%>
			</table>
		</form>
<%--	</form>--%>
	</body>
</html>
