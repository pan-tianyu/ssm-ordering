<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title></title>
	<style type="text/css">
		.div{
			list-style: none;
			height: 100px;
			width: 200px;
			border: 1px #FF0000 solid;
			border-radius: 4px;
			float: right;
			background-color: antiquewhite;
		}
		.div div{
			height: 50px;
			width: 100%;
			border-radius: 4px;
			background-color: white;
		}
		.ul li{
			list-style: none;
			font-size: 10px;
		}
		.li a{
			font-size: 8px;
		}
	</style>
</head>
<body>
<div style="width: 850px;margin: auto;">
	<div style="list-style:none;height: 855px; width:600px;border: 1px #FF0000 solid;border-radius: 4px;margin-left: 20px;">
		<c:forEach var="menu" items="${requestScope.get('list')}">
			<div style="list-style:none;height: 180px; width:290px;border: 1px #FF0000 solid;border-radius: 4px;float: left;"><!--左侧-->
				<form action="${pageContext.request.contextPath}/ordering/addOrdering" method="post">
				<div><!--第一行左侧-->

					<div style="width: 150px;height: 140px;"><!--图片-->
						${menu.getPicture()}
					</div>
					<button style="margin-top: 5px;margin-left: 50px;" type="submit">放入餐车</button>
				</div>
				<div style="width: 130px;margin-top: -170px;float: right;"><!--右侧-->

					<li class="li"><a name="name"> 菜名:${menu.getMenuName()}</a></li>
					<li class="li"><a> 市场价格:${menu.getPrice()}</a></li>
					<li class="li"><a> 会员价格:${menu.getUnit_price()}</a></li>
					<li class="li"><a> 配料:${menu.getMaterial()}</a></li>
					<br />

					<li class="li"><a> 菜品类型:${menu.getState()}</a></li>

				</div>
				</form>
			</div>
		</c:forEach>

	</div>




	<div style="height: 200px;width: 300px;float: right;margin-top: -855px;">
		<div class="div">
			<div>
<%--				<div style="height: 50px;width: 30px;float: left;">--%>
<%--					图片--%>
<%--				</div>--%>
				<h1 align="center"style="font-size: 20px;float: left;margin-top: 15px;color: coral;">餐厅公告</h1>
				<li style="float: right;list-style: none; margin-top: 20px;">
					<a href="" style="text-align: right;font-size: 5px;text-decoration: none;">更多>></a>
				</li>
				<hr style="width:180px;border-top:0.05px black dashed;" />

				<tr>
					<th>编号</th>
					<th>公告标题</th>
					<th>公告时间</th>

				</tr>

				<c:forEach var="notice" items="${requestScope.get('list1')}">
					<tr>
						<td>${notice.getId()}</td>
						<td>${notice.getTitle()}</td>
						<td>${notice.getTime()}</td>

					</tr>
				</c:forEach>


				<div style="width: auto;height: auto;background-image: url(<%=request.getContextPath()%>/img/43.gif);background-color:black;float: left;">
				</div>
				<ul class="ul" >
					<li>新增菜单</li>
					<li>本店特色</li>
				</ul>

			</div>

		</div>
		<div style="margin-top: 130px;margin-right: -200px;" class="div";>
			<div>
<%--				<div style="height: auto;width: 30px;float: left;">--%>
<%--					图片--%>
<%--				</div>--%>
				<h1 align="center"style="font-size: 20px;float: left;margin-top: 15px;color: coral;">我的餐车</h1>
				<li style="float: right;list-style: none; margin-top: 20px;">
					<a href="" style="text-align: right;font-size: 5px;text-decoration: none;">详细>></a>
				</li>
				<hr style="width:180px;border-top:0.05px black dashed;" />
			</div>
			<div style="background-color: lightpink;">

			</div>
			<button style="background-image: url(<%=request.getContextPath()%>/img/quxiao2.gif);width: 77px;height: 20px;float: right;margin-top: 20px;"></button>
			<button style="background-image: url(<%=request.getContextPath()%>/img/canche_submit.gif);width: 54px;height: 20px;float: right;margin-top: 20px;"></button>

		</div>
		<div style="margin-top: 280px;margin-right: -200px;" class="div";>
			<div>
<%--				<div style="height: 10px;width: 30px;float: left;">--%>
<%--					图片--%>
<%--				</div>--%>
				<h1 align="center"style="font-size: 20px;float: left;margin-top: 5px;color: coral;">销售排行榜</h1>
				<hr style="width:180px;border-top:0.05px black dashed;" />
			</div>
			<ul class="ul" style="margin-top: 1px;">
				<li>粉蒸肉</li>
				<li>粉蒸肉</li>
			</ul>
		</div>
	</div>
</div>
</body>
</html>

