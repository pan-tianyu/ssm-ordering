<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
		<link href="<%=request.getContextPath()%>/img/topmenu_bg.jpg" rel="stylesheet" type="text/img"/>
		<link href="<%=request.getContextPath()%>/img/logo.gif" rel="stylesheet" type="text/img"/>
		<link href="<%=request.getContextPath()%>/img/menu_hover.jpg" rel="stylesheet" type="text/img"/>
		<link href="<%=request.getContextPath()%>/img/menu.jpg" rel="stylesheet" type="text/img"/>
			<style>
				* {
					margin: 0px;
					padding: 0px;
				}/*将所有边界值设置为0px，即取消各个边框之间的间距*/	
				.header {
					width: 850px;
					height: 110px;
					margin: auto;
				}/*头部界面设置*/
				.topnav {
					width: 395px;
					height: 25px;
					background-image: url(<%=request.getContextPath()%>/img/topmenu_bg.jpg);
				}/*头部按钮设置*/
				.msg {
					width: 350px;
					height: 25px;
				}/*登录后信息提示*/
				.logo {
					width: 211px;
					height: 84px;
					background-image: url(<%=request.getContextPath()%>/img/logo.gif);
				}/*头部图片logo*/
				.menunav {
					width: 630px;
					height: 41px;
				}/*菜单布局设置*/
				.menunav li a{
					text-decoration: none;
				}
				.header nav,
				.header div {
					float: left;
				}
				.header .topnav {
					float: right;
				}
				.header .menunav {
					margin-top: 14px;
				}
				
				.header .msg {
					margin-left: 490px;
					margin-top: 5px;
				}
				
				.header .logo {
					margin-top: -30px;
				}
				
				.topnav ul {
					margin-left: 65px;
				}
				
				.topnav ul li {
					float: left;
					list-style: none;
					margin: 3px;
					font-family: "微软雅黑";
					font-size: 10px;
					color: gray;
				}
				
				.topnav ul li a:hover {
					color: orange;
				}
				.topnav ul li a{
					text-decoration: none;
				}
				.menunav ul li {
					float: left;
					list-style: none;/*去除li标签的点*/
					width: 87px;
					height: 41px;
					background-image: url(<%=request.getContextPath()%>/img/menu.jpg);
					margin-left: 3px;
				}
				
				.menunav ul li a {
					display: block;
					width: 87px;
					height: 41px;
					text-align: center;
					line-height: 41px;
					color: white;
					font-weight: 900;
					font-size: 14px;
				}
				
				.menunav ul li:hover {
					background-image: url(<%=request.getContextPath()%>/img/menu_hover.jpg);
				}
				
				.msg {
					color: orange;
				}
				.xian{
					width:700px;
					height:5px;
					color:orange;
					margin: auto;
				}
			</style>
	</head>
	<body>
	<form action="${pageContext.request.contextPath}/fore/header" method="post">
							<!--头部-->
			<header class="header">
				<nav class="topnav"><!--nav导航按钮-->
					<ul>
						<li>
							<a href="${pageContext.request.contextPath}/user/login" target="article">会员登录</a> |
						</li>
						<li>
							<a href="${pageContext.request.contextPath}/user/register" target="article">会员注册</a> |
						</li>
						<li>
							<a>注销退出</a> |
						</li>
						<li>
							<a href="${pageContext.request.contextPath}/fore/delivery" target="article">配送说明</a> |
						</li>
						<li>
							<a href="${pageContext.request.contextPath}/fore/aboutUs" target="article">关于我们</a>
						</li>
					</ul>
				</nav>
				<div class="msg">亲爱的${customer},欢迎光临！</div>
				<div class="logo"></div>
				<nav class="menunav">
					<ul>
						<li>
							<a href="${pageContext.request.contextPath}/fore/home" target="article">首页</a>
						</li>
						<li>
							<a href="${pageContext.request.contextPath}/fore/buffetCar" target="article">我的餐车</a>
						</li>
						<li>
							<a href="${pageContext.request.contextPath}/ordering/myOrder" target="article">我的订单</a>
						</li>
						<li>
							<a href="${pageContext.request.contextPath}/fore/userCenter" target="article">用户中心</a>
						</li>
						<li>
							<a href="${pageContext.request.contextPath}/fore/delivery" target="article">配送说明</a>
						</li>
						<li>
							<a href="${pageContext.request.contextPath}/fore/aboutUs" target="article">关于我们</a>
						</li>
						<li>
							<a href="${pageContext.request.contextPath}/customer/logout" target="article">注销退出</a>
						</li>
					</ul>
				</nav>
				<div class="xian"></div>
			</header>
	</form>
	</body>
</html>
