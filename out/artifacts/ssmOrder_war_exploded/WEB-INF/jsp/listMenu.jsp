<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<table border="1" cellspacing="0" align="center">
		<tr>
			<td>序号</td>
			<td>菜品名称</td>
			<td>原料</td>	
			<td>菜品类别</td>
			<td>单价</td>
			<td>会员单价</td>
			<td>说明</td>
			<td>图片</td>
			<td>操作</td>
		</tr>
		<c:forEach items="${list }" var="d">
			<tr>
				<td>${d.id }</td>
				<td>${d.menuname }</td>
				<td>${d.material }</td>
				<td>${d.typeid}</td>
				<td>${d.price}</td>
				<td>${d.unit_price}</td>
				<td>${d.state}</td>
				<td><img src="../img/${d.picture}"/></td>
				<td>
					<a href="${pageContext.request.contextPath}/Back/menu/MenuUpdate?id=${d.id }" style="color: darkorange;"style="text-decoration: none;">修改</a>
					<a href="MenuDelete?id=${d.id }" style="color: red;" style="text-decoration: none;">删除</a></td>
					
				</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>