<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<table cellspacing="0" align="center" border="2px"style="width: 100%;">
	<input type="hidden" name="option" value="listNotice"/>
		<tr>
			<td colspan="5" style="text-align: center;">公告信息列表</td>
		</tr>
		<tr>
			<td>     </td>
			<td>公告标题</td>
			<td>公告内容</td>
			<td>发布时间</td>
			<td>操作</td>
		</tr>
		<c:forEach items="${list }" var="t">
			<tr>
				<td>${t.id }</td>
				<td>${t.title }</td>
				<td>${t.concept }</td>
				<td>${t.time }</td>
				<td><a href="${pageContext.request.contextPath}/Back/notice/NoticeUpdate?id=${t.id }" style="color: darkorange;"style="text-decoration: none;">修改</a> <a
					href="NoticeDelete?id=${t.id }" style="color: red;" style="text-decoration: none;">删除</a></td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>